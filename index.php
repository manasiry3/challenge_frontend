<?php
class Records {
	public $legal_name = "CrowdApex Systems Inc";
	public $business_name = "CrowdApex";
	public $business_address = "290 Bremner Blvd, Toronto, ON M5V 3L9, Canada";
	public $business_mailing_address = " Wellington St, Ottawa, ON K1A 0A9, Canada";
	public $business_phone = "+1 647 944 3023"; 
	public $business_fax = "+1 647 944 3024";
	public $business_email = "hello@controlledgoods.com";
	public $application_type = "New";
	public $business_title = "Employee";
	public $lang = "English";
}
$controlledGoods = [
	["Description"=>'Miliatry Product'	,"Group"=>123,"Item"=>123.1],
	["Description"=>"Silly Product"		,"Group"=>345,"Item"=>321.0],
	["Description"=>'Miliatry Product'	,"Group"=>123,"Item"=>123.1],
	["Description"=>"Silly Product"		,"Group"=>345,"Item"=>321.0],
	["Description"=>'Miliatry Product'	,"Group"=>123,"Item"=>123.1],

];

$model = new Records();

?>

<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Security Screen</title>

  <link rel="stylesheet" href="css/styles.css">
  <link rel="stylesheet" href="theme/bootstrap/css/bootstrap.min.css">

</head>

<body>


  	<?= $model->legal_name ?>  <br>

	<?php foreach($controlledGoods as $good): ?>

	<?= $good['Description'] ?> <?=$good['Group']?> <?=$good['Item']?> <br>

	<?php endforeach;?>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="total-wrap">
				<ol class="heading-wrap" type="A">
					<li class="bg-one"><h1>Business information (To be completed by the Designated Official)</h1></li>
					<ol type='1' class="border-no">
						<div class="border-black">
							<li>
								<label>Legal Name</label>
								<input type="text" class="form-control"/>
							</li>
						</div>
						<div class="border-black">
						<li>
							<label>Business Name (If diferent from legal name)</label>
							<input type="text" class="form-control"/>
						</li>
						</div>
						<div class="border-black">
						<li>
							<label>Civic Address</label>
							<input type="text" class="form-control"/>
						</li>
						</div>
						<div class="border-black">
						<li>
							<label>mailing Address (If diferent from civic address)</label>
							<input type="email" class="form-control"/>
						</li>
						</div>
						<div class="row m-0 border-black no-gutters">
							<li class="col-md-6 border-right-black">
								<label>Telephone Number (Include extension no. if applicable) </label>
								<input type="text" class="form-control"/>
							</li>
							<li class="col-md-6">
								<label>Facsimile Number</label>
								<input type="text" class="form-control"/>
							</li>
						</div>
						<div class="border-black">
						<li>
							<label>E-mail</label>
							<input type="email" class="form-control"/>
						</li></div>
						<div class="border-black">
						<li>
							<label>Description of the controlled goods the applicant may be required to examine, possess or transfer (Refer to the Export Control List (Ea))</label>
							<table class="table table-responsive-md table-bordered">
								<thead>
									<tr>
										<th scope="col" colspan="2">Description of Controlled Goods</th>
										<th scope="col">ECL Group No.</th>
										<th scope="col">ECL Item No.</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th scope="row">a</th>
										<td><input type="text" class="form-control"/></td>
										<td><input type="text" class="form-control"/></td>
										<td><input type="text" class="form-control"/></td>
									</tr>
									<tr>
										<th scope="row">b</th>
										<td><input type="text" class="form-control"/></td>
										<td><input type="text" class="form-control"/></td>
										<td><input type="text" class="form-control"/></td>
									</tr>
									<tr>
										<th scope="row">c</th>
										<td><input type="text" class="form-control"/></td>
										<td><input type="text" class="form-control"/></td>
										<td><input type="text" class="form-control"/></td>
									</tr>
									<tr>
										<th scope="row">d</th>
										<td><input type="text" class="form-control"/></td>
										<td><input type="text" class="form-control"/></td>
										<td><input type="text" class="form-control"/></td>
									</tr>
									<tr>
										<th scope="row">e</th>
										<td><input type="text" class="form-control"/></td>
										<td><input type="text" class="form-control"/></td>
										<td><input type="text" class="form-control"/></td>
									</tr>
								</tbody>
								</table>
						</li>
						</div>
					</ol>
					<li class="bg-two">
						APPLICANT INFORMATION (To be completed by the applicant)
					</li>
					<div class="border-black  border-top-0 border-left-0 border-right-0">
					<ol start="9">
						<li class="border-black border-bottom-0">
							<label>Type of Application </label>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
								<label class="form-check-label" for="inlineCheckbox1">New</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
								<label class="form-check-label" for="inlineCheckbox2">Re-Assessment</label>
							</div>

						</li>
						<li class="border-black border-bottom-0">
							<label>Business Title (Select all that apply) </label>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option1">
								<label class="form-check-label" for="inlineCheckbox1">	Owner</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="option2">
								<label class="form-check-label" for="inlineCheckbox2">Authorized Individual</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="checkbox" id="inlineCheckbox5" value="option2">
								<label class="form-check-label" for="inlineCheckbox2">Designated Official</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="checkbox" id="inlineCheckbox6" value="option2">
								<label class="form-check-label" for="inlineCheckbox2">Officer</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="checkbox" id="inlineCheckbox6" value="option2">
								<label class="form-check-label" for="inlineCheckbox2">Director</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="checkbox" id="inlineCheckbox7" value="option2">
								<label class="form-check-label" for="inlineCheckbox2">Employee</label>
							</div>
						</li >
						<li class="border-black  border-bottom-0">
							<label>Preferred Language of Correspondence </label>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="checkbox" id="inlineCheckbox8" value="option1">
								<label class="form-check-label" for="inlineCheckbox1">English</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="checkbox" id="inlineCheckbox9" value="option2">
								<label class="form-check-label" for="inlineCheckbox2">French</label>
							</div>

						</li>
					</ol>
					</div>
				</ol>
				</div>
			</div>
		</div>
	</div>
	  <script src="js/scripts.js"></script>
<script src="theme/bootstrap/js/bootstrap.min.js"></script>
<script src="theme/bootstrap/js/popper.min.js"></script>
</body>
</html>

